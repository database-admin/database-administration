# Mysql 

**MySQL** es un sistema de gestión de bases de datos relacionales (SGBD) de código abierto, reconocido por su robustez, flexibilidad y facilidad de uso. Desarrollado bajo la licencia dual GPL/Comercial por Oracle Corporation, se posiciona como la opción líder en el ámbito open source, y una de las más populares en general, junto a titanes como Oracle Database y Microsoft SQL Server.
